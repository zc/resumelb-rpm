import hashlib

class Wrapper:

    def __init__(self, data):
        self.data = data
        self.accessed = set()

        # Read crucial keys the recipe probably won't
        self.get('version')
        self.get('type')

    def __contains__(self, key):
        return key in self.data

    def __getitem__(self, key):
        r = self.data[key]
        if isinstance(r, unicode):
            r = str(r)
        self.accessed.add(key)
        return r

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    @property
    def digest(self):
        return '*'+hashlib.md5(repr(
            [(key, self.data[key]) for key in sorted(self.accessed)]
            )).hexdigest()
