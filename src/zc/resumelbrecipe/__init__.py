import hashlib
import zc.metarecipe
import zc.resumelbrecipe.optionssignature
import zc.zk

setting_names = ('backlog_warning', 'backlog_error',
                 'worker_backlog_warning', 'worker_backlog_error',
                 'worker_age_warning', 'worker_age_error',
                 )

class Recipe(zc.metarecipe.Recipe):

    def __init__(self, buildout, name, options):
        super(Recipe, self).__init__(buildout, name, options)
        assert name.endswith('.0'), name # There can be only one.

        name = name[:-2]
        path = '/'+name.replace(',', '/')

        zk = zc.zk.ZK('zookeeper:2181')
        options = zc.resumelbrecipe.optionssignature.Wrapper(
            zk.properties(path))

        user = self.user = options.get('user', 'zope')
        port = options['port']

        self['deployment'] = dict(
            recipe = 'zc.recipe.deployment',
            name=name,
            user=user,
            )

        self.parse("""
            [logging.conf]
            recipe = zc.recipe.deployment:configuration
            deployment = deployment
            text =
               <logger>
                  level INFO
                  <logfile>
                     path ${deployment:log-directory}/event.log
                     format %(asctime)s %(name)s %(levelname)s %(message)s
                     old-files 10
                     when MIDNIGHT
                  </logfile>
               </logger>

               <logger accesslog>
                 level info
                 name accesslog
                 propagate false

                 <logfile>
                   path ${deployment:log-directory}/access.log
                   format %(message)s
                   old-files 10
                   when MIDNIGHT
                 </logfile>
               </logger>
               """)

        classifier = options.get(
            'classifier',
            "zc.resumelb.classlesspool:classifier"
            " -p zc.resumelb.classlesspool:ClasslessPool")

        self['lb'] = dict(
            recipe = 'zc.zdaemonrecipe',
            deployment = 'deployment',
            program = (
                '${buildout:bin-directory}/zkresumelb %(options)s'
                ' -s${deployment:run-directory}/lb-status.sock'
                ' -L${logging.conf:location} -laccesslog'
                ' %(classifier)s -a:%(port)s'
                ' zookeeper:2181 %(path)s'
                % dict(
                    options = options.get('options', ''),
                    path = path,
                    port = port,
                    classifier = classifier and ('-r %s' % str(classifier)),
                    )),
            **{'zdaemon.conf': """
                <runner>
                start-test-program nc -z localhost %s
                </runner>
                """ % port,
               'shell-script': 'true',
               })

        settings = "\n  ".join("%s = %s" % (s, options[s])
                               for s in setting_names
                               if s in options)

        urlpath = str(options.get('monitor-path', '/'))

        self['cimaa'] = dict(
            name = '/etc/cimaa/monitors.d/%s.cfg' % name,
            recipe = 'zc.recipe.deployment:configuration',
            deployment = 'deployment',
            text = cimaa_text % dict(port=port, urlpath=urlpath),
            )

        self.parse(
            agent_conf_template % dict(
                name=name,
                port=port,
                digest=options.digest,
                urlpath=urlpath,
                settings=settings
                ),
            )

cimaa_text = """
[lb]
command =
  /opt/resumelb/bin/rlb-nagios -m \\
    ${deployment:run-directory}/lb-status.sock
thresholds =
  workers warning < 3 error < 2 clear > 2
  mean_backlog warning > 3 error > 9 clear < 5
  max_backlog warning > 9 error > 30 clear < 20
  max_age warning > 3 error > 500 clear < 30
nagios_performance = true

[http]
command = ${buildout:bin-directory}/http-check http://localhost:%(port)s%(urlpath)s
"""

agent_conf_template = '''
[%(name)s-zimagent.cfg]
recipe = zc.recipe.deployment:configuration
directory = /etc/zim/agent.d/
deployment = deployment
text =
  [%(name)s]
  class = zim.resumelbmonitor.Monitor
  deployment = ${deployment:run-directory}
  instances = lb

  %(settings)s

  [%(name)s-http]
  class = zim.httpmonitor.monitor.Monitor
  description = Resumelb HTTP monitor for %(name)s
  url = http://localhost:%(port)s%(urlpath)s
  uri = %(name)s/%(port)s/root_http

[rc]
recipe = zc.recipe.rhrc
deployment = deployment
parts = lb
process-management = true
digest = %(digest)s
'''
