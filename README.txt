Deployment project for deploying resumelb in ZC
***********************************************

Changes
*******

1.1.1 2015-03-25
=================

- Fix deployment of the CIMAA HTTP monitor.

1.1.0 2015-03-24
=================

- Add CIMAA monitor for the HTTP port.

1.0.5 2015-03-11
=================

- Fixed: monitor max_age metric was -1 when there were no outstanding
  requests.  Together with a CIMAA metric parsing bug, this caused
  spurious alerts for missing metrics.

- Fixed: Bad metric name in monitor conf

1.0.4 2015-03-10
=================

- Fixed: CIMAA configuration files were formatted incorrectly.

1.0.3 2015-03-08
=================

- Fixed: didn't say the magic word: nagios_performance = true
  to get metrics.

1.0.2 2015-03-03
=================

- Fixed: didn't use the shell-script option when generating RC
  scripts, causing pain and agony when updating zdaemon.

- Don't alert on max-age unless it's really high. (We may regret even that.)

- Get resumelb fixes, especially worker links

1.0.1 2015-03-01
=================

- Fixed: Didn't pass lb status socket to nagios plugin.

- Fixed: lb registered by host name rather than ip.

1.0.0 2015-03-01
=================

- Default to a classless pool.

- cimaa monitoring configuration

0.10.6 2014-10-27
=================

Fixed: Write logging.conf to the deployment's etc directory.

0.10.5 2014-10-27
=================

Use zc.zk 2.1.0, for better zk stability.

0.10.4 2014-08-28
=================

NFC

0.10.3 2014-06-04
=================

Use zc.resumelb 0.7.3

0.10.2 2014-06-02
=================

Use zc.resumelb 0.7.2, with keep alives to detect workers that went
away uncleanly.

Use released gevent.

Fixed: digest computation didn't take type and version into account.

0.10.1 2014-02-12
=================

Fixed: zc.zk was missing monitor.zcml.

0.10.0 2014-02-11
=================

- Updated to use kazoo to talk to ZooKeeper.

0.9.1 2013-12-20
================

Fixed: Missing dependency on zim.resumelbmonitor

Fixed: The resumelb monitor looked in the wrong location for the lb
       status socket.

0.9.0 2013-12-13
================

Fixed: Monitoring was broken:

       The configuration file had an invalid extension and was ignored by
       zimagent.

Added the ability to control monitoring thresholds.

Only include options read by the recipe in the digest.  This will
allow run-time parameters to be updated in the tree without provoking
restarts.

0.8.1 2013-11-26
================

User newer deployment recipe to get logs in the right place.
